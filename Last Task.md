Last Task
=========

Cuando usar Triggers?

Un trigger (o disparador) en una base de datos, es un procedimiento 
que se ejecuta cuando se cumple una determinada condicion al realizar 
una operacion. Dependiendo de la base de datos, los triggers pueden ser 
de insercion (INSERT), actualizacion (UPDATE) o borrado (DELETE).
Algunas bases de datos pueden ejecutar triggers al crear, borrar o editar
usuarios, tablas, bases de datos u otros objetos.

USOS:
* Nos permite registrar, auditar y monitorear los procesos de cambio de valores
  a las tablas de base de datos activas.
* Pueden validar los valores aprobando o negando acciones realizadas por las
  sentencias de tipo MDL.
* Puede preservar la consistencia y claridad de los valores ejecuatando acciones
  relacionados con los objetos de tipo tabla de la base de datos activa.