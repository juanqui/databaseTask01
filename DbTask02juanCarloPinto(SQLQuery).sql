create database Task02
USE Task02
create table Persona  ( persona_id INT NOT NULL, nombre VARCHAR(50), apellido VARCHAR(50), genero VARCHAR(1), fechaNacimiento date, pais varchar(50));
create table Libro  ( libro_id INT NOT NULL, titulo VARCHAR(50), precio INT NULL, persona_id VARCHAR(50));
--delete Persona
--1 Registrar 7 personas con datos completos 
INSERT INTO Persona 
VALUES	(1, 'Ana', 'Almeida',	'F', '1990-02-14', 'Colombia'),
		(2, 'Bill', 'Bush',	'M', '1970-02-14', 'Mexico'),
		(3, 'Jorge', 'Gonzales', 'M', '1995-10-21', 'Bolivia'),
		(4, 'Ricahrd', 'Stallman',	'M', '1953-03-16', 'EEUU'),
		(5, 'Linus', 'Torvalds',	'M', '1969-07-24', 'Finlandia'),
		(6, 'Steve', 'Wozniak',	'M', '1950-08-11', 'EEUU'),
		(7, 'Dennis', 'Ritchie',	'M', '1970-09-09', 'EEUU');

--2 Registrar 3 personas sin nacionalidad
INSERT INTO Persona (persona_id, nombre, apellido, genero, fechaNacimiento)
VALUES	(8, 'Ana', 'Almeida',	'F', '1990-02-14'),
		(9, 'Marco', 'Pinto',	'M', '1970-02-14'),
		(10, 'Jorge', 'Lopez', 'M', '1995-10-21');

--3 Registrar 1 persona sin nacionalidad ni fecha de nacimiento
INSERT INTO Persona (persona_id, nombre, apellido, genero)
VALUES	(8, 'Ana', 'Almeida',	'F');

--4 Registrar 10 libros
INSERT INTO Libro(libro_id, titulo, precio, persona_id)
VALUES	(1, 'Learn SQLserver 2014', 57.00, 1),
		(2, 'C++ for Really Programmenrs', 45, 2),
		(3, 'Transact Begining to Profesional', 55, 3),
		(4, 'Angular 5 en 21 dias', 65, 4),
		(5, 'Calculo Programando con C#', 75, 5),
		(6, 'SQL Server', 85, 1),
		(7, 'TyoeScript Step by Step', 130, 5);

--5 Listar el nombre y apellido de la tabla �Persona� ordenados por apellido y luego por nombre.
select p.nombre, p.apellido
from Persona as p
order by p.apellido, p.nombre

--6 Listar los libros con precio mayor a 50 Bs. y que el titulo del libro empieze con la letra �A�
select *
from Libro
where precio > 50  and titulo like 'A%'

--7 Listar el nombre y apellido de las personas que tengan al menos un libro cuyo precio sea mayor a 50 Bs.
select    Persona.Nombre, Libro.titulo, Libro.precio
from     Persona, Libro
where    Libro.persona_id = Persona.persona_id  and Libro.precio > 50  
 
--8 Listar el nombre y apellido de las personas que nacieron el a�o 1990
select nombre, apellido
from Persona
where fechaNacimiento like '1990-%-%' --sale duplicado
--where fechaNacimiento BETWEEN '1990-01-01' and '1990-12-31'  --sale duplicado

--9 Listar los libros donde el titulo del libro contenga la letra �e� en la segunda posici�n
select *
from Libro
where titulo like '_e%' 

--10 Contar el numero de registros de la tabla �Persona�
select count(*)
from Persona

--11 Listar el o los libros m�s baratos
select min(Libro.precio)
from Libro

--12 Listar el o los libros m�s caros   
select MAX (precio) MaxPrecio
from Libro
   
--13 Mostrar el promedio de precios de los libros

select sum(precio) / count(*)
--select avg(precio)
from Libro

--14 Mostrar la sumatoria de los precios de los libros
select sum(precio)
from Libro

--15 Modificar la longitud del campo titulo para que soporte hasta titulos de 300 caracteres
alter table Libro
alter column titulo VARCHAR(300);

--16 Listar las personas que sean del pa�s de Colombia � de Mexico
select * 
from Persona 
where Persona.pais = 'Colombia' OR Persona.pais = 'Mexico';

--17 Listar los libros que no empiezen con la letra �T�
select * 
from Libro 
where NOT LEFT(Libro.titulo, 1)='T';

--18 Listar los libros que tengan un precio entre 50 a 70 Bs.
select * 
from Libro 
where precio BETWEEN 50 AND 70

--19 Listar las personas AGRUPADOS por pa�s
select  pais, COUNT(*) as Cantidad 
from Persona 
group by pais;

--20 Listar las personas AGRUPADOS por pa�s y genero
select pais, genero, COUNT(*) as Cantidad 
FROM Persona 
GROUP BY pais, genero;

--21 Listar las personas AGRUPADOS por genero, cuya cantidad sea mayor a 5
select genero, COUNT(*) as Cantidad 
from Persona 
group by genero
having COUNT(persona_id) > 5;

--22 Listar los libros ordenados por titulo en orden descendente
select * 
from Libro
order by titulo desc;

--23 Eliminar el libro con el titulo �SQL Server�
delete from Libro
where Titulo ='SQL Server'

--24 Eliminar los libros que comiencen con la letra �A�
delete from Libro
where LEFT(Titulo, 1) = 'A';

--25 Eliminar las personas que no tengan libros **
delete from Persona
where persona_id NOT IN (select persona_id from Libro);

--26 Eliminar todos los libros
delete Libro

--27 Modificar el nombre de la persona llamada �Marco� por �Marcos�
update Persona set nombre='Marcos'
where nombre='Marco'

--28 Modificar el precio del todos los libros que empiezen con la palabra �Calculo�  a  57 Bs.
update Libro SET precio = 57.00
where titulo like 'Calculo %'

--29 Inventarse una consulta con GROUP BY  y HAVING  y explicar el resultado que retorna.
select  p.genero, count(l.precio) as CantLibros  --, (getdate() - p.fechaNacimiento)
from Persona as p, Libro as l
where p.persona_id = l.persona_id 
group by p.genero
having (count(l.precio)>0)-- and (count(p.persona_id)>2)
/*
la consulta anterior muestra las personas agrupadas por genero, de las cuales se cuenta la cantidad de libros que existen en cada genero.
*/